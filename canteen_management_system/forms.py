from django import forms
from canteen_management_system.models import FoodItem



class FoodItemForm(forms.Form):
    name = forms.CharField()
    price = forms.IntegerField()
    qty = forms.IntegerField()
    discount = forms.FloatField()


class FoodItemModelForm(forms.ModelForm):
    class Meta:
        model = FoodItem
        fields = '__all__'

