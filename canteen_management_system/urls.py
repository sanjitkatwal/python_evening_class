from canteen_management_system import views
from django.urls import path

app_name = 'cms'


urlpatterns = [
    path('home/', views.home, name='home'),
    path('list_food/', views.list_food, name='list_food'),
    path('add_food/', views.add_food, name='add_food'),
    path('edit_food/<int:id>/', views.edit_food, name='edit_food'),
    path('delete_food/<int:id>/', views.delete_food, name='delete_food')
]