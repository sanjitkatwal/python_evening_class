from django.contrib import admin
from canteen_management_system.models import Cook, CookInfo


# Register your models here.
admin.site.register(Cook)
admin.site.register(CookInfo)