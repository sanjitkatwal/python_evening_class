from django.shortcuts import render, HttpResponse, redirect
from canteen_management_system.models import FoodItem
from canteen_management_system.forms import FoodItemForm
from canteen_management_system.forms import FoodItemModelForm

def home(request):
    # data come from model, design come from template
    #return HttpResponse('<h2>hey im from home</h2>')

    list_friends = ['ram', 'hari', 'sita']
    return render(request, 'home.html', context={'list_friends': list_friends})


def list_food(request):
    list_food=FoodItem.objects.all()
    return render(request, 'food/list_food.html', context={'list_food': list_food})


def add_food(request):
    if request.method == 'GET':
        # food_form = FoodItemForm()
        food_form = FoodItemModelForm()
        return render(request, 'add_form.html', {'food_form': food_form})
    elif request.method == 'POST':
        food_form = FoodItemModelForm(request.POST)
        if food_form.is_valid():
            food_form.save()
            # FoodItem.objects.create(
            #     name=request.POST['name'],
            #     qty=request.POST['qty'],
            #     price=request.POST['price'],
            #     discount=request.POST['discount'],
            # )
        #return HttpResponse(request.POST)
            return redirect('cms:list_food')
        else:
            return render(request, 'add_form.html', {'food_form': food_form})


def edit_food(request, id):
    food = FoodItem.objects.get(id=id)
    if request.method == 'GET':
        form = FoodItemModelForm(instance=food)
        return render(request, 'edit_form.html', {'form': form})
    elif request.method == 'POST':
        form = FoodItemModelForm(request.POST, instance=food)
        if form.is_valid():
            form.save()
            return redirect('cms:list_food')
        else:
            return render(request, 'edit_form.html', {'form': form})


def delete_food(request, id):
    food = FoodItem.objects.get(id=id)
    food.delete()
    return redirect('cms:list_food')
